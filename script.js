window.onload = function() {
    const e = document.querySelector("form");
    const ins = document.getElementById("search");
    const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if (isMobile){
        ins.autofocus = false;
    };
    list()
    /*ins.addEventListener("keydown", function(event){
        if (event.key === 'Enter') {
            //do this
            let eingabe = document.getElementById("search").value.toLowerCase();
            list(eingabe)
        };

    })*/
    //window.open("https://google.com/", "_self")
};

/*document.getElementById("search").onkeydown = function(){
    let eingabe = document.getElementById("search").value;
    tmp = eingabe.split("")
    if (tmp[0] != "$"){
        tmp.unshift("$ ")
    }
    var text=""
    for (let i=0; i<tmp.length; i++){
        text+=tmp[i]
    }
    document.getElementById("search").value=String(text)
}
//Geht nicht aufgrund der Verschiebung der Array indicies
*/

function list(){
    let com = document.getElementById("search").value.toLowerCase();
    let tmp = com.split(" ");
    //list of websides and commands
    //The ls or help command for a page of command lists
    if (com == "ls" || com =="help"){
        //alert("daa");
        window.open("commands/index.html", "_self");
    }
    //The search command is for searching with Ecosia
    if (tmp[0] == "search"){
        let suche = tmp[1]
        for (let i=2; i<tmp.length; i++){
            suche=suche.concat("_" + tmp[i])
            //alert(suche)
        }
        suche=String("https://www.ecosia.org/search?q=" + suche)
        window.open(suche, "_blank")
    }
    if (com == "root"){
        window.open("subpages/sample.html", "_self")
        //alert("root")
    }
}

//Copy Code
function openInNewTab(href) {
    Object.assign(document.createElement('a'), {
      target: '_self',
      href: href,
    }).click();
  }
//
function fireClickEvent(element) {
    var evt = new window.MouseEvent('click', {
        view: window,
        bubbles: true,
        cancelable: true
    });

    element.dispatchEvent(evt);
}

function openNewURLInTheSameWindow(targetURL) {
    var a = document.createElement('a');
    a.href = targetURL;
    fireClickEvent(a);
}